# waypass

Password prompt for wayland.

Read manpage for keybindings and usage flags.

## Usage

Stdin is the description shown above the input field

Also, you can insert a line of highlighted standout text with the
`-n` (notice) flag. This line appears right below the description
and above the input field:

```
echo description | waypass -n 'important info'
```

## Building

Run `samu` or `ninja` in the repo directory.
Configure built-in settings in config.h

## Future plans

Integrate this program's functionality into the uprompt binary.
