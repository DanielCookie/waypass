#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <sys/mman.h>
#include <unistd.h>
#include <xkbcommon/xkbcommon.h>
#include <assert.h>
#include <sys/timerfd.h>
#include <fontconfig.h>
#include <pixman.h>
#include <fcntl.h>
#include <fcft/fcft.h>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"

#include "utf8.h"
#include "pool_buffer.h"
#include "config.h"
#include "icon_bits/icons.h"
#include "icon_bits/key.h"
/* #include "icon_bits/padlock.h" */

enum {
	RunFailure = -1,
	RunSuccess = 0,
	RunRunning = 1,
};

enum {
	ExitSuccess = 0,
	ExitFailure = 1,
	ExitTimedOut = 2,
};

enum {
	ModeHidden = 1,
	ModeDisableNaked = 2,
	ModeBottom = 8,
};

struct desc_line {
	struct fcft_text_run *run;
	struct desc_line *next;
};

struct output {
	struct waypass_state *waypass;
	struct wl_output *output;
	struct zxdg_output_v1 *xdg_output;
	int32_t scale;
};

struct pix_canvas {
	pixman_color_t bg;     pixman_color_t pass_bg;
	pixman_color_t cursor; pixman_color_t border;
	pixman_color_t notice; pixman_color_t desc;
	pixman_color_t prompt; pixman_color_t help;

	pixman_image_t *pass;
	pixman_image_t *rune;  pixman_image_t *rune_alt;
};

struct hex_colors {
	uint32_t bg;      uint32_t desc;   uint32_t notice;
	uint32_t pass_bg; uint32_t prompt; uint32_t pass;
	uint32_t cursor;  uint32_t help;   uint32_t border;
	uint32_t rune;    uint32_t rune_alt;
	uint32_t image_layers[3];
};

struct dimensions {
	uint8_t  line_height;  uint8_t  text_level;
	uint8_t  padding;      uint8_t  border_width;
	uint16_t width;        uint16_t height;
	uint16_t prompt_width; uint16_t help_width;
	uint16_t input_width;  uint16_t upper_padding;
	uint16_t input_start_y;uint16_t input_start_x;
};

struct waypass_state {
	struct output *output;
	char *output_name;

	struct wl_display *display;
	struct wl_compositor *compositor;
	struct wl_shm *shm;
	struct wl_seat *seat;
	struct wl_keyboard *keyboard;
	struct zwlr_layer_shell_v1 *layer_shell;
	struct zxdg_output_manager_v1 *output_manager;

	struct wl_surface *surface;
	struct zwlr_layer_surface_v1 *layer_surface;

	struct xkb_context *xkb_context;
	struct xkb_keymap *xkb_keymap;
	struct xkb_state *xkb_state;

	struct pool_buffer buffers[2];
	struct pool_buffer *current;

	struct hex_colors hex;
	struct dimensions dim;
	struct pix_canvas canvas;

	const char **fn_names[FONT_NUMBER];
	char asterisk_char;
	const char *prompt_str;
	const char *help_str;
	const char *notice_str;
	struct fcft_text_run *prompt_run;
	struct fcft_text_run *help_run;
	struct fcft_font *font;

	char text[BUFSIZ];
	size_t cursor;

	int32_t repeat_timer;
	int32_t repeat_delay;
	int32_t repeat_period;
	uint32_t repeat_key;
	enum wl_keyboard_key_state repeat_key_state;
	xkb_keysym_t repeat_sym;

	uint8_t mode;
	int8_t run;

	struct desc_line *description;
	uint8_t desc_lines;
	uint16_t desc_width;

	void (*draw_input_hidden) (struct waypass_state *, pixman_image_t *, uint16_t, uint16_t);
	void (*draw_input) (struct waypass_state *, pixman_image_t *, uint16_t, uint16_t);
};

// Principal functions
static void waypass_init(struct waypass_state *ws, struct dimensions *dim);
static void font_init(struct waypass_state *ws);
static inline void surface_fini(struct waypass_state *ws);
static inline void canvas_fini(struct pix_canvas *canvas);
static inline void font_fini(struct waypass_state *ws);
static inline void compositor_fini(struct waypass_state *ws);
static struct pix_canvas canvas_init(struct hex_colors *hex);
static void waypass_fini(struct waypass_state *ws);
static void insert(struct waypass_state *ws, const char *s, ssize_t n);
static void get_description_from_stdin(struct waypass_state *ws);
static void free_description(struct waypass_state *ws);
static void draw_frame(struct waypass_state *ws);
static void waypass_create_surface(struct waypass_state *ws);
static void keypress(struct waypass_state *ws,
	enum wl_keyboard_key_state key_state, xkb_keysym_t sym);
static size_t next_rune(struct waypass_state *ws, int8_t incr);
static inline void type_rune(struct waypass_state *ws,
	xkb_keysym_t sym, char *buf);

static void noop() {}

static void eprintf(const char *fmt, ...) {
	va_list ap;

	fprintf(stderr, "%s: ", PROGNAME);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

static inline void fill_box_solid(pixman_image_t *pixman, pixman_color_t *color,
uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2) {

	pixman_image_fill_boxes(PIXMAN_OP_SRC, pixman, color, 1,
		&(pixman_box32_t) {.x1 = x1, .x2 = x2, .y1 = y1, .y2 = y2});
}

static inline void draw_glyph(pixman_image_t *pixman,
const struct fcft_glyph *rune, pixman_image_t *fill, uint16_t x, uint16_t y) {

	if (pixman_image_get_format(rune->pix) == PIXMAN_a8r8g8b8) {
		pixman_image_composite32(PIXMAN_OP_OVER, rune->pix, NULL, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
	else {
		pixman_image_composite32(PIXMAN_OP_OVER, fill, rune->pix, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
}

static inline const pixman_color_t hex2pixman(uint32_t hex_color) {
	return (pixman_color_t) {
		.red   = (hex_color >> 0x18 & 0xFF) * 0xFF,
		.green = (hex_color >> 0x10 & 0xFF) * 0xFF,
		.blue  = (hex_color >> 0x8  & 0xFF) * 0xFF,
		.alpha = (hex_color         & 0xFF) * 0xFF,
	};
}

static inline int produce_rand_num(int limit) {
	return rand() % (limit + 1);
} 

static void parse_hex_color(char *str, uint32_t *color) {
	if (str[0] == '#') str++;

	size_t len = strnlen(str, BUFSIZ);

	if (len != 6 && len != 8)
		eprintf("Color format must be '[#]rrggbb[aa]'\n");

	char *ptr;
	uint32_t _val = strtoul(str, &ptr, 16);
	if (*ptr != '\0') eprintf("Could not convert string to hex number");

	*color = len == 6 ? ((_val << 8) | 0xff) : _val;
}

static void parse_color_triad(char *str, uint32_t *colors) {
	char *p, *comma;
	size_t i;

	for (p = str, i = 0; i < 3; p = comma + 1, i++) {
		comma = strchr(p, ',');
		if (comma != NULL) *comma = '\0';

		parse_hex_color(p, colors + i);
	}
}

static void set_line_height(struct dimensions *dim, char *height_str) {
	uint8_t i;
	for (i = 0; i <= 3; i++) if (height_str[i] == ':') break;

	if (2 < i) dim->line_height = atoi(height_str);
	else {
		height_str[i] = '\0';
		dim->line_height = atoi(height_str);
		dim->text_level = dim->line_height - atoi(height_str + ++i);
	}
}

static void surface_enter(void *data, struct wl_surface *surface,
struct wl_output *wl_output) {

	struct waypass_state *ws = data;
	ws->output = wl_output_get_user_data(wl_output);
	wl_surface_set_buffer_scale(ws->surface, ws->output->scale);
	wl_surface_commit(ws->surface);
	draw_frame(ws);
}

static const struct wl_surface_listener surface_listener = {
	.enter = surface_enter,
	.leave = noop,
};

static void layer_surface_configure(void *data, struct zwlr_layer_surface_v1 *surface,
uint32_t serial, uint32_t width, uint32_t height) {

	struct waypass_state *ws = data;
	ws->dim.width = width;
	ws->dim.height = height;
	zwlr_layer_surface_v1_ack_configure(surface, serial);
}

static void layer_surface_closed(void *data,
struct zwlr_layer_surface_v1 *surface) {

	struct waypass_state *ws = data;
	ws->run = RunSuccess;
}

static struct zwlr_layer_surface_v1_listener layer_surface_listener = {
	.configure = layer_surface_configure,
	.closed = layer_surface_closed,
};

static void output_scale(void *data,
struct wl_output *wl_output, int32_t factor) {

	struct output *output = data;
	output->scale = factor;
}

static void output_name(void *data, struct zxdg_output_v1 *xdg_output,
const char *name) {

	struct output *output = data;
	struct waypass_state *ws = output->waypass;
	char *outname = ws->output_name;

	if (!ws->output && outname && strcmp(outname, name) == 0) ws->output = output;
}

static struct wl_output_listener output_listener = {
	.geometry = noop,
	.mode = noop,
	.done = noop,
	.scale = output_scale,
};

static struct zxdg_output_v1_listener xdg_output_listener = {
	.logical_position = noop,
	.logical_size = noop,
	.done = noop,
	.name = output_name,
	.description = noop,
};

static void keyboard_keymap(void *data, struct wl_keyboard *wl_keyboard,
uint32_t format, int32_t fd, uint32_t size) {

	struct waypass_state *ws = data;

	if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
		close(fd);
		ws->run = RunFailure;
		return;
	}

	char *map_shm = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (map_shm == MAP_FAILED) {
		close(fd);
		ws->run = RunFailure;
		return;
	}

	ws->xkb_keymap = xkb_keymap_new_from_string(ws->xkb_context,
		map_shm, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	munmap(map_shm, size);
	close(fd);
	ws->xkb_state = xkb_state_new(ws->xkb_keymap);
}

static void keyboard_repeat(struct waypass_state *ws) {
	keypress(ws, ws->repeat_key_state, ws->repeat_sym);
	struct itimerspec spec = { 0 };

	spec.it_value.tv_sec = ws->repeat_period / 1000;
	spec.it_value.tv_nsec = (ws->repeat_period % 1000) * 1000000l;
	timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
}

static void keyboard_key(void *data, struct wl_keyboard *wl_keyboard,
uint32_t serial, uint32_t time, uint32_t key, uint32_t _key_state) {

	struct waypass_state *ws = data;
	enum wl_keyboard_key_state key_state = _key_state;
	xkb_keysym_t sym = xkb_state_key_get_one_sym(ws->xkb_state, key + 8);

	keypress(ws, key_state, sym);

	if (key_state == WL_KEYBOARD_KEY_STATE_PRESSED && 0 <= ws->repeat_period) {
		ws->repeat_key_state = key_state;
		ws->repeat_sym = sym;
		ws->repeat_key = key;

		struct itimerspec spec = { 0 };
		spec.it_value.tv_sec = ws->repeat_delay / 1000;
		spec.it_value.tv_nsec = (ws->repeat_delay % 1000) * 1000000l;
		timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
	}
	else if (key_state == WL_KEYBOARD_KEY_STATE_RELEASED && key == ws->repeat_key) {
		struct itimerspec spec = { 0 };
		timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
	}
}

static void keyboard_repeat_info(void *data,
struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {

	struct waypass_state *ws = data;
	ws->repeat_delay = delay;

	if (0 < rate) ws->repeat_period = 1000 / rate;
	else ws->repeat_period = -1;
}

static void keyboard_modifiers(void *data, struct wl_keyboard *keyboard,
uint32_t serial, uint32_t mods_depressed, uint32_t mods_latched,
uint32_t mods_locked, uint32_t group) {

	struct waypass_state *ws = data;
	xkb_state_update_mask(ws->xkb_state, mods_depressed, mods_latched,
		mods_locked, 0, 0, group);
}

static const struct wl_keyboard_listener keyboard_listener = {
	.keymap = keyboard_keymap,
	.enter = noop,
	.leave = noop,
	.key = keyboard_key,
	.modifiers = keyboard_modifiers,
	.repeat_info = keyboard_repeat_info,
};

static void seat_capabilities(void *data, struct wl_seat *seat,
enum wl_seat_capability caps) {

	struct waypass_state *ws = data;

	if (caps & WL_SEAT_CAPABILITY_KEYBOARD) {
		ws->keyboard = wl_seat_get_keyboard(seat);
		wl_keyboard_add_listener(ws->keyboard, &keyboard_listener, ws);
	}
}

static const struct wl_seat_listener seat_listener = {
	.capabilities = seat_capabilities,
	.name = noop,
};

static void handle_global(void *data, struct wl_registry *registry,
uint32_t name, const char *interface, uint32_t version) {

	struct waypass_state *ws = data;

	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		ws->compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, 4);
	}
	else if (strcmp(interface, wl_shm_interface.name) == 0) {
		ws->shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
	}
	else if (strcmp(interface, wl_seat_interface.name) == 0) {
		struct wl_seat *seat = wl_registry_bind(registry, name,
			&wl_seat_interface, 4);
		wl_seat_add_listener(seat, &seat_listener, ws);
	}
	else if (strcmp(interface, zwlr_layer_shell_v1_interface.name) == 0) {
		ws->layer_shell = wl_registry_bind(registry, name,
			&zwlr_layer_shell_v1_interface, 1);
	}
	else if (strcmp(interface, zxdg_output_manager_v1_interface.name) == 0) {
		ws->output_manager = wl_registry_bind(registry, name,
			&zxdg_output_manager_v1_interface, 3);
	}
	else if (strcmp(interface, wl_output_interface.name) == 0) {
		struct output *output = calloc(1, sizeof(struct output));

		output->output = wl_registry_bind(registry, name, &wl_output_interface, 3);
		output->waypass = ws;
		output->scale = 1;

		wl_output_set_user_data(output->output, output);
		wl_output_add_listener(output->output, &output_listener, output);

		if (ws->output_manager != NULL) {
			output->xdg_output = zxdg_output_manager_v1_get_xdg_output(
				ws->output_manager, output->output);
			zxdg_output_v1_add_listener(output->xdg_output,
				&xdg_output_listener, output);
		}
	}
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = noop,
};

void draw_first_frame(struct waypass_state *ws, bool bottom) {
	ws->current = get_next_buffer(ws->shm, ws->buffers,
		ws->dim.width, ws->dim.height, ws->output ? ws->output->scale : 1);

	if (!ws->current) return;

	pixman_image_t *pix_buf = ws->current->pixman, *fill, *key;
	pixman_color_t key_col;
	struct fcft_font *font = ws->font;
	struct pix_canvas *c = &ws->canvas;
	struct dimensions *d = &ws->dim;
	uint16_t x, y = 0, left_anchor;
	size_t i;
	const struct fcft_glyph *rune_p;
	struct fcft_text_run *run;
	uint32_t codepoint, state = UTF8_ACCEPT;
	struct desc_line *desc_line;

	left_anchor = d->width / 4;

	fill_box_solid(pix_buf, &c->bg, 0, d->width,
		bottom * d->border_width, d->height - d->border_width);
	fill_box_solid(pix_buf, &c->border, 0, d->width,
		!bottom * (d->height - d->border_width),
		bottom * d->border_width + !bottom * d->height);

	y += bottom * d->border_width + d->padding + d->upper_padding;

	// Description
	for (fill = pixman_image_create_solid_fill(&c->desc),
	     desc_line = ws->description; desc_line;
	     desc_line = desc_line->next, y += d->line_height) {

		run = desc_line->run;
		x = left_anchor + d->padding;

		for (i = 0, rune_p = *run->glyphs; i < run->count;
			x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

			draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
		}
	}
	pixman_image_unref(fill);

	// Notice
	if (ws->notice_str) {
		x = left_anchor + d->padding;
		fill = pixman_image_create_solid_fill(&c->notice);

		for (char *p = (char *)ws->notice_str; *p; p++) {
			if (utf8_decode(&state, &codepoint, *p)) continue;

			rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
			draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
			x += rune_p->advance.x;
		}

		pixman_image_unref(fill);
		y += d->line_height;
	}

	// Help
	y += 2 * d->padding + d->line_height;
	run = ws->help_run;

	x = left_anchor + d->padding;
	for (fill = pixman_image_create_solid_fill(&c->help),
	     i = 0, rune_p = *run->glyphs; i < run->count;
	     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

		draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
	}
	pixman_image_unref(fill);

	// Prompt
	y -= d->padding + d->line_height;
	run = ws->prompt_run;

	x = left_anchor + d->padding;
	for (fill = pixman_image_create_solid_fill(&c->prompt),
	     i = 0, rune_p = *run->glyphs; i < run->count;
	     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

		draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
	}
	pixman_image_unref(fill);

	d->input_start_y = y;
	d->input_start_x = x + d->padding;

	// Image
	y = bottom * d->border_width +
	    (d->height - d->border_width) / 2 - ImageHeight / 2;
	x = left_anchor - 2 * d->padding - ImageWidth;

	for (i = 0; i < 3; i++) {
		key_col = hex2pixman(ws->hex.image_layers[i]);
		fill = pixman_image_create_solid_fill(&key_col);

		key = pixman_image_create_bits(PIXMAN_a8r8g8b8, ImageWidth,
		      ImageHeight, (uint32_t *)ImageLayers[i], ImageWidth * 4);

		pixman_image_composite32(PIXMAN_OP_OVER, fill, key, pix_buf,
			0, 0, 0, 0, x, y, ImageWidth, ImageHeight);

		pixman_image_unref(key);
		pixman_image_unref(fill);
	}

	wl_surface_attach(ws->surface, ws->current->buffer, 0, 0);
	wl_surface_damage_buffer(ws->surface, 0, 0, ws->dim.width, ws->dim.height);
	wl_surface_commit(ws->surface);
}

void draw_input_naked(struct waypass_state *ws,
pixman_image_t *pixman, uint16_t x, uint16_t y) {

	struct dimensions *d = &ws->dim; 
	struct pix_canvas *c = &ws->canvas;
	const struct fcft_glyph *rune_p;
	uint16_t input_limit = x + d->input_width - 2 * d->padding, cursor_pos = x;
	uint32_t state = UTF8_ACCEPT, codepoint;
	char *str_p;
	size_t i;

	for (i = 1, str_p = ws->text; *str_p; str_p++, i++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		if (input_limit < x) {
			cursor_pos = x;
			break;
		}

		rune_p = fcft_rasterize_char_utf32(ws->font, codepoint, FCFT_SUBPIXEL_NONE);
		draw_glyph(pixman, rune_p, c->pass, x, y + d->text_level);

		x += rune_p->advance.x;
		if (i == ws->cursor) cursor_pos = x;
	}

#if UNDERSCORE_CURSOR
	fill_box_solid(pixman, &c->cursor,
		cursor_pos, cursor_pos + 9, y + d->text_level + 1, y + d->text_level + 3);
#else
	fill_box_solid(pixman, &c->cursor,
		cursor_pos, cursor_pos + 2, y + 4, y + d->line_height - 4);
#endif
}

static void draw_input_hidden(struct waypass_state *ws,
pixman_image_t *pixman, uint16_t x, uint16_t y) {

	struct dimensions *d = &ws->dim; 
	struct pix_canvas *c = &ws->canvas;
	uint16_t input_limit = x + d->input_width - 2 * d->padding, cursor_pos = x;
	char *str_p;
	size_t i;
	const struct fcft_glyph *asterisk;
	uint32_t state = UTF8_ACCEPT, codepoint;

	asterisk = fcft_rasterize_char_utf32(ws->font,
		ws->asterisk_char, FCFT_SUBPIXEL_NONE);

	for (i = 1, str_p = ws->text; *str_p; str_p++, i++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		if (input_limit < x) {
			cursor_pos = x;
			break;
		}

		draw_glyph(pixman, asterisk, c->pass, x, y + d->text_level);

		x += asterisk->advance.x;
		if (i == ws->cursor) cursor_pos = x;
	}

#if UNDERSCORE_CURSOR
	fill_box_solid(pixman, &c->cursor,
		cursor_pos, cursor_pos + 9, y + d->text_level + 1, y + d->text_level + 3);
#else
	fill_box_solid(pixman, &c->cursor,
		cursor_pos, cursor_pos + 2, y + 4, y + d->line_height - 4);
#endif
}

void draw_input_hidden_len_icons(struct waypass_state *ws,
pixman_image_t *pixman, uint16_t x, uint16_t y) {

	struct dimensions *d = &ws->dim;
	struct pix_canvas *c = &ws->canvas;
	pixman_image_t *icon;
	size_t i;
	bool curr_pair;

	pixman_image_t *fills[] = {c->rune, c->rune_alt};
	y += (d->line_height - IconHeight) / 2;

	for (curr_pair = 0, i = 0; i < 3 + produce_rand_num(6);
	     i++, x += d->line_height + d->padding / 4, curr_pair = !curr_pair) {

		icon = pixman_image_create_bits(PIXMAN_a8r8g8b8, IconWidth,
			IconHeight, (uint32_t *)Icons[produce_rand_num(IconAmount - 1)],
			IconWidth * 4);

		pixman_image_composite32(PIXMAN_OP_OVER, fills[curr_pair], icon, pixman,
			0, 0, 0, 0, x, y, IconWidth, IconHeight);

		pixman_image_unref(icon);
	}
}

void draw_input_hidden_len(struct waypass_state *ws,
pixman_image_t *pixman, uint16_t x, uint16_t y) {

	struct dimensions *d = &ws->dim;
	struct pix_canvas *c = &ws->canvas;
	size_t i;
	const struct fcft_glyph *rune;
	bool curr_pair;

	for (curr_pair = 0, i = 0; i < 5 + produce_rand_num(25);
	     i++, x += rune->advance.x, curr_pair = !curr_pair) {

	    rune = fcft_rasterize_char_utf32(ws->font,
	    	HiddenLenChars[produce_rand_num(HiddenCharAmount - 1)],
	    	FCFT_SUBPIXEL_NONE);

		draw_glyph(pixman, rune, c->pass, x, y + d->text_level);
	}
}

void draw_input_hidden_len_no_feedback(struct waypass_state *ws,
pixman_image_t *pixman, uint16_t x, uint16_t y) {

#if UNDERSCORE_CURSOR
	fill_box_solid(pixman, &ws->canvas.cursor,
		x, x + 9, y + ws->dim.text_level + 1, y + ws->dim.text_level + 3);
#else
	fill_box_solid(pixman, &ws->canvas.cursor,
		x, x + 2, y + 4, y + ws->dim.line_height - 4);
#endif
}

void draw_frame(struct waypass_state *ws) {
	ws->current = get_next_buffer(ws->shm, ws->buffers,
		ws->dim.width, ws->dim.height, ws->output ? ws->output->scale : 1);

	if (!ws->current) return;

	pixman_image_t *pix_buf = ws->current->pixman;

	struct pix_canvas *c = &ws->canvas;
	struct dimensions *d = &ws->dim;

	// Input background
	fill_box_solid(pix_buf, &c->pass_bg,
		d->input_start_x, d->input_start_x + d->input_width,
		d->input_start_y, d->input_start_y + d->line_height);

	// Input text
	ws->draw_input(ws, pix_buf, d->input_start_x + d->padding, d->input_start_y);

	wl_surface_attach(ws->surface, ws->current->buffer, 0, 0);
	wl_surface_damage(ws->surface, d->input_start_x,
		d->input_start_y, ws->dim.input_width, d->line_height);
	wl_surface_commit(ws->surface);
}

inline void type_rune(struct waypass_state *ws, xkb_keysym_t sym, char *buf) {
	if (xkb_keysym_to_utf8(sym, buf, 8)) {
		insert(ws, buf, strnlen(buf, 8));
		draw_frame(ws);
	}
}

size_t next_rune(struct waypass_state *ws, int8_t incr) {
	size_t len = strlen(ws->text), n = ws->cursor + incr;

	for (; n < len && (ws->text[n] & 0xC0) == 0x80; n += incr);
	return n;
}

void insert(struct waypass_state *ws, const char *s, ssize_t n) {
	if (sizeof ws->text - 1 < strlen(ws->text) + n) return;

	memmove(ws->text + ws->cursor + n, ws->text + ws->cursor,
		sizeof ws->text - ws->cursor - (0 < n ? n : 0));

	if (n > 0) memcpy(ws->text + ws->cursor, s, n);

	ws->cursor += n;
}

void keypress(struct waypass_state *ws, enum wl_keyboard_key_state key_state,
xkb_keysym_t sym) {

	if (key_state != WL_KEYBOARD_KEY_STATE_PRESSED) return;

	bool ctrl = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_CTRL, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool shift = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_SHIFT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool alt = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_ALT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);

	char buf[8];
	size_t len = strlen(ws->text);

	switch (sym) {
	case XKB_KEY_KP_Enter:
	case XKB_KEY_Return:
		puts(ws->text);
		fflush(stdout);
		ws->run = RunSuccess;
		break;

	case XKB_KEY_Left:
		if (ws->cursor) {
			ws->cursor = ctrl ? 0 : next_rune(ws, -1);
			draw_frame(ws);
		}
		break;

	case XKB_KEY_Right:
		if (ws->cursor < len) {
			ws->cursor = ctrl ? len : next_rune(ws, +1);
			draw_frame(ws);
		}
		break;

	case XKB_KEY_BackSpace:
		if (!ws->cursor) break;
		insert(ws, NULL,
			((alt || ctrl) ? 0 : next_rune(ws, -1)) - ws->cursor);
		draw_frame(ws);
		break;

	case XKB_KEY_Delete:
		if (ws->cursor == len) break;
		size_t cursor_pos = ws->cursor;
		ws->cursor = (alt || ctrl) ? len : next_rune(ws, +1);
		insert(ws, NULL, cursor_pos - ws->cursor);
		draw_frame(ws);
		break;

	case XKB_KEY_J:
		if (ctrl) {
			puts(ws->text);
			fflush(stdout);
			ws->run = RunSuccess;
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_j:
		if (ctrl) {
			puts(ws->text);
			fflush(stdout);
			ws->run = RunSuccess;
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_b:
		if (!ctrl && !alt) type_rune(ws, sym, buf);
		else {
			if (!ws->cursor) break;
			ws->cursor = ctrl ? next_rune(ws, -1) : 0;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_f:
		if (!ctrl && !alt) type_rune(ws, sym, buf);
		else {
			if (len <= ws->cursor) break;
			ws->cursor = ctrl ? next_rune(ws, +1) : len;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_a:
		if (ctrl) {
			ws->cursor = 0;
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_e:
		if (ctrl) {
			ws->cursor = len;
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_l:
		if (ctrl) {
			if (ws->cursor < len) ws->cursor = len;
			insert(ws, NULL, -len);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_u:
		if (ctrl) {
			if (ws->cursor < 1) break;
			insert(ws, NULL, -ws->cursor);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_k:
		if (ctrl) {
			if (ws->cursor == len) break;
			size_t cursor_pos = ws->cursor;
			ws->cursor = len;
			insert(ws, NULL, cursor_pos - len);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_w:
		if (ctrl) {
			if (!ws->cursor) break;
			insert(ws, NULL, 0 - ws->cursor);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_d:
		if (ctrl) {
			if (ws->cursor == len) break;
			ws->cursor = next_rune(ws, +1);
			insert(ws, NULL, next_rune(ws, -1) - ws->cursor);
			draw_frame(ws);
		}
		else if (alt) {
			if (ws->cursor == len) break;
			size_t cursor_pos = ws->cursor;
			ws->cursor = len;
			insert(ws, NULL, cursor_pos - ws->cursor);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_Y:
		if (ctrl) {
			char cbd_content[MaxClipboardLen];
			int16_t i, cmd_len;
			for (i = 0; i < MaxClipboardLen; i++) cbd_content[i] = '\0';

			FILE *cbd_f = popen(ClipboardCommand, "r");
			if (cbd_f == NULL) {
				insert(ws, "'Nothing'", 6);
				draw_frame(ws);
				break;
			}

			fread(cbd_content, MaxClipboardLen, 1, cbd_f);
			pclose(cbd_f);

			cmd_len = strnlen(cbd_content, MaxClipboardLen);
			for (i = cmd_len - 1; 0 <= i; i--) {
				if (cbd_content[i] == '\n' || cbd_content[i] == ' ' ||
				    cbd_content[i] == '\t') cbd_content[i] = '\0';
				else {
					cmd_len = i + 1;
					break;
				}
			}

			for (i--; 0 <= i; i--) if (cbd_content[i] == '\n') cbd_content[i] = ' ';
			insert(ws, cbd_content, cmd_len);
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_x:
		if (ctrl) {
			if (ws->mode & ModeDisableNaked) type_rune(ws, sym, buf);
			else if (ws->mode & ModeHidden) {
				ws->draw_input = draw_input_naked;
				ws->mode ^= ModeHidden;
			}
			else {
				ws->draw_input = ws->draw_input_hidden;
				ws->mode ^= ModeHidden;
			}
			draw_frame(ws);
		}
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_bracketleft:
	case XKB_KEY_c:
		if (ctrl) ws->run = RunFailure;
		else type_rune(ws, sym, buf);
		break;

	case XKB_KEY_Escape:
		ws->run = RunFailure;
		break;

	default:
		type_rune(ws, sym, buf);
	}
}

void waypass_create_surface(struct waypass_state *ws) {
	ws->surface = wl_compositor_create_surface(ws->compositor);
	wl_surface_add_listener(ws->surface, &surface_listener, ws);
	ws->layer_surface = zwlr_layer_shell_v1_get_layer_surface(
		ws->layer_shell, ws->surface, NULL, ZWLR_LAYER_SHELL_V1_LAYER_TOP, PROGNAME);
	assert(ws->layer_surface != NULL);

	uint32_t anchor =
		ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT;

	anchor |= ws->mode & ModeBottom ?
		ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM : ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP; 

	zwlr_layer_surface_v1_set_anchor(ws->layer_surface, anchor);
	zwlr_layer_surface_v1_set_size(ws->layer_surface, 0, ws->dim.height);
	zwlr_layer_surface_v1_set_exclusive_zone(ws->layer_surface, -1);
	zwlr_layer_surface_v1_set_keyboard_interactivity(ws->layer_surface, true);
	zwlr_layer_surface_v1_add_listener(ws->layer_surface,
		&layer_surface_listener, ws);

	wl_surface_commit(ws->surface);
	wl_display_roundtrip(ws->display);
}

inline void canvas_fini(struct pix_canvas *canvas) {
	pixman_image_unref(canvas->pass);
	pixman_image_unref(canvas->rune);
	pixman_image_unref(canvas->rune_alt);
}

struct pix_canvas canvas_init(struct hex_colors *hex) {
	const pixman_color_t fill_pass   = hex2pixman(hex->pass);
	const pixman_color_t fill_rune     = hex2pixman(hex->rune);
	const pixman_color_t fill_rune_alt = hex2pixman(hex->rune_alt);

	return (struct pix_canvas) {
		.bg         = hex2pixman(hex->bg),
		.pass_bg    = hex2pixman(hex->pass_bg),
		.cursor     = hex2pixman(hex->cursor),
		.border     = hex2pixman(hex->border),
		.notice     = hex2pixman(hex->notice),
		.desc       = hex2pixman(hex->desc),
		.prompt     = hex2pixman(hex->prompt),
		.help       = hex2pixman(hex->help),
		.pass       = pixman_image_create_solid_fill(&fill_pass),
		.rune       = pixman_image_create_solid_fill(&fill_rune),
		.rune_alt   = pixman_image_create_solid_fill(&fill_rune_alt),
	};
}

void waypass_init(struct waypass_state *ws, struct dimensions *dim) {
	dim->padding = dim->line_height / 2;

	uint32_t height_from_key = ImageHeight + 2 * dim->padding,
	         height = (2 + ws->desc_lines) * dim->line_height + 4 * dim->padding;

	if (height < height_from_key) {
		dim->height = height_from_key + dim->border_width;
		dim->upper_padding = height_from_key - height;
	} else {
		dim->height = height + dim->border_width;
		dim->upper_padding = 0;
	}

	dim->height = height < height_from_key ? height_from_key : height;
	dim->height += dim->border_width + (ws->notice_str ? dim->line_height : 0);

	ws->canvas = canvas_init(&ws->hex);

	ws->display = wl_display_connect(NULL);
	if (!ws->display) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		eprintf("Could not connect to wayland display\n");
	}

	ws->xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!ws->xkb_context) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		wl_display_disconnect(ws->display);
		eprintf("Could not create xkb context\n");
	}

	ws->repeat_timer = timerfd_create(CLOCK_MONOTONIC, 0);
	assert(0 <= ws->repeat_timer);

	struct wl_registry *registry = wl_display_get_registry(ws->display);
	wl_registry_add_listener(registry, &registry_listener, ws);
	wl_display_roundtrip(ws->display);

	assert(ws->compositor != NULL);
	assert(ws->layer_shell != NULL);
	assert(ws->shm != NULL);
	assert(ws->output_manager != NULL);

	// Second roundtrip for xdg-output
	wl_display_roundtrip(ws->display);

	if (ws->output_name && !ws->output) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		compositor_fini(ws);
		eprintf("Output %s not found\n", ws->output_name);
	}
}

void font_init(struct waypass_state *ws) {
	uint16_t advance;
	size_t i, rune_num;
	char *str_p;
	uint32_t state = UTF8_ACCEPT, codepoint;

	fcft_init(FCFT_LOG_COLORIZE_AUTO, 8, FCFT_LOG_CLASS_ERROR);
	fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3);

	ws->font = fcft_from_name(FONT_NUMBER, (const char **)(*ws->fn_names), NULL);
	if (!ws->font) eprintf("Could not load font[s]!\n");

	uint32_t prompt32[strlen(ws->prompt_str) - 1];
	uint32_t help32[strlen(ws->help_str) - 1];

	// Make prompt run
	for (rune_num = 0, str_p = (char *)ws->prompt_str; *str_p; str_p++) {
		if (utf8_decode(&state, &codepoint, *str_p)) continue;

		prompt32[rune_num] = codepoint;
		rune_num++;
	}

	ws->prompt_run = fcft_rasterize_text_run_utf32(ws->font,
		rune_num, (const uint32_t *)prompt32, FCFT_SUBPIXEL_NONE);

	for (advance = 0, i = 0; i < rune_num; i++) {
		advance += ws->prompt_run->glyphs[i]->advance.x;
	}
	ws->dim.prompt_width = advance + 2 * ws->dim.padding;

	// Make help run
	for (rune_num = 0, str_p = (char *)ws->help_str; *str_p; str_p++) {

		if (utf8_decode(&state, &codepoint, *str_p)) continue;
		help32[rune_num] = codepoint;
		rune_num++;
	}

	ws->help_run = fcft_rasterize_text_run_utf32(ws->font,
		rune_num, (const uint32_t *)help32, FCFT_SUBPIXEL_NONE);

	for (advance = 0, i = 0; i < rune_num; i++) {
		advance += ws->help_run->glyphs[i]->advance.x;
	}
	ws->dim.help_width = advance + 2 * ws->dim.padding;
}

inline void surface_fini(struct waypass_state *ws) {
	zwlr_layer_surface_v1_destroy(ws->layer_surface);
	wl_surface_destroy(ws->surface);
	zwlr_layer_shell_v1_destroy(ws->layer_shell);
}

void free_description(struct waypass_state *ws) {
	struct desc_line *line;
	for (line = ws->description; line; line = line->next) {
		fcft_text_run_destroy(line->run);
		//free(line);
	}
}

inline void font_fini(struct waypass_state *ws) {
	fcft_text_run_destroy(ws->prompt_run);
	fcft_text_run_destroy(ws->help_run);
	fcft_destroy(ws->font);
	fcft_fini();
}

inline void compositor_fini(struct waypass_state *ws) {
	wl_compositor_destroy(ws->compositor);
	wl_shm_destroy(ws->shm);
	wl_keyboard_destroy(ws->keyboard);
	zxdg_output_manager_v1_destroy(ws->output_manager);
	wl_display_disconnect(ws->display);
}

void waypass_fini(struct waypass_state *ws) {
	canvas_fini(&ws->canvas);
	free_description(ws);
	font_fini(ws);
	surface_fini(ws);
	compositor_fini(ws);
}

void get_description_from_stdin(struct waypass_state *ws) {
	char buf[sizeof ws->text], *p, *str_p;
	struct desc_line *line, **end;
	uint32_t state = UTF8_ACCEPT, codepoint;
	uint16_t advance;
	size_t rune_num, i, len;

	for (end = &ws->description; fgets(buf, sizeof buf, stdin);) {
		if((p = strchr(buf, '\n'))) *p = '\0';

		len = strlen(buf);
		if (!len) continue;

		uint32_t buf32[len - 1];

		line = malloc(sizeof *line);
		if (!line) return;

		for (rune_num = 0, str_p = buf; *str_p; str_p++) {
			if (utf8_decode(&state, &codepoint, *str_p)) continue;

			buf32[rune_num] = codepoint;
			rune_num++;
		}

		line->run = fcft_rasterize_text_run_utf32(ws->font,
			rune_num, buf32, FCFT_SUBPIXEL_NONE);

		for (i = 0, advance = 0; i < rune_num; i++) {
			advance += line->run->glyphs[i]->advance.x;
		}

		if (ws->desc_width < advance) ws->desc_width = advance;
	    *end = line; end = &line->next; ws->desc_lines++; line->next = NULL;
	}
}

void alarm_handler(int sig) {
	fprintf(stderr, "timed out!\n");
	exit(ExitTimedOut);
}

/*
 * MAIN
 */

int main(int argc, char **argv) {
	struct waypass_state ws = {
		.fn_names = FnNames,

		.hex = {
			.bg      = ColBg,     .desc   = ColDesc,   .notice = ColNotice,
			.pass_bg = ColPassBg, .prompt = ColPrompt, .pass   = ColPass,
			.cursor  = ColCursor, .help   = ColHelp,   .border = ColBorder,
			.rune    = ColRune,   .rune_alt = ColRuneAlt,
			.image_layers  = {ColImageFg, ColImageShading, ColImageBorder},
		},
		.dim = {
			.line_height  = DimLineHeight,
			.text_level   = DimTextLevel,
			.input_width  = DimInputWidth,
			.border_width = BorderWidth,
		},

		.asterisk_char = Asterisk,
		.prompt_str = PromptText,
		.help_str = HelpText,
		.notice_str = NULL,

		.mode = ModeHidden,
		.draw_input_hidden = draw_input_hidden,
		.draw_input = draw_input_hidden,
		.desc_width = 0,

		.run = RunRunning,
	};

	const char *usage =
		"Usage: waypass [-v] [-b] [-dn] [-hs,hr,hd] [-fn font]\n"
		"       [-o output] [-p prompt] [-n notice] [-a asterisk]\n"
		"       [-h height[:text_level]] [-bw width] [-t timeout]\n"
		"       [-bg,bc,dc,pc,ib,ic,hc color] [-rc,ra color] [-Ic {colors}]\n";

	for (size_t i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-v")) { puts(VERSION); return 0; }
		if (!strcmp(argv[i], "-b")) ws.mode |= ModeBottom;
		else if (!strncmp(argv[i], "-h", 2)) {

			switch (argv[i][2]) {
			case 's': ws.draw_input_hidden = draw_input_hidden_len; break;
			case 'r': ws.draw_input_hidden = draw_input_hidden_len_icons; break;
			case 'd': ws.draw_input_hidden = draw_input_hidden_len_no_feedback; break;
			default: goto print_usage;
			}
			ws.draw_input = ws.draw_input_hidden;

			time_t t;
			srand((unsigned) time(&t));
		}
		else if (!strcmp(argv[i], "-dn")) {
			ws.help_str = HelpTextDisableNaked;
			ws.mode |= ModeDisableNaked;
		}
		else if (i == argc - 1) goto print_usage;
		else if (!strcmp(argv[i], "-bw")) ws.dim.border_width = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-a")) ws.asterisk_char = *argv[++i];
		else if (!strcmp(argv[i], "-h")) set_line_height(&ws.dim, argv[++i]);
		else if (!strcmp(argv[i], "-p")) ws.prompt_str = argv[++i];
		else if (!strcmp(argv[i], "-n")) ws.notice_str = argv[++i];
		else if (!strcmp(argv[i], "-o")) ws.output_name = argv[++i];
		else if (!strcmp(argv[i], "-t")) {
			signal(SIGALRM, alarm_handler);
			alarm(atoi(argv[++i]));
		}
		else if (!strcmp(argv[i], "-fn")) *(ws.fn_names)[0] = argv[++i];
		else if (!strcmp(argv[i], "-bg")) parse_hex_color(argv[++i], &ws.hex.bg);
		else if (!strcmp(argv[i], "-bc")) parse_hex_color(argv[++i], &ws.hex.border);
		else if (!strcmp(argv[i], "-dc")) parse_hex_color(argv[++i], &ws.hex.desc);
		else if (!strcmp(argv[i], "-nc")) parse_hex_color(argv[++i], &ws.hex.notice);
		else if (!strcmp(argv[i], "-pc")) parse_hex_color(argv[++i], &ws.hex.prompt);
		else if (!strcmp(argv[i], "-ib")) parse_hex_color(argv[++i], &ws.hex.pass_bg);
		else if (!strcmp(argv[i], "-ic")) parse_hex_color(argv[++i], &ws.hex.pass);
		else if (!strcmp(argv[i], "-hc")) parse_hex_color(argv[++i], &ws.hex.help);
		else if (!strcmp(argv[i], "-rc")) parse_hex_color(argv[++i], &ws.hex.rune);
		else if (!strcmp(argv[i], "-ra")) parse_hex_color(argv[++i], &ws.hex.rune_alt);
		else if (!strcmp(argv[i], "-Ic")) parse_color_triad(argv[++i], ws.hex.image_layers);
		else goto print_usage;
	}

	font_init(&ws);
	get_description_from_stdin(&ws);
	waypass_init(&ws, &ws.dim);
	waypass_create_surface(&ws);

	draw_first_frame(&ws, ws.mode & ModeBottom);

	struct pollfd fds[] = {
		{ wl_display_get_fd(ws.display), POLLIN },
		{ ws.repeat_timer, POLLIN },
	};

	const int nfds = sizeof(fds) / sizeof(*fds);

	while (ws.run == RunRunning) {
		errno = 0;

		do {
			if (wl_display_flush(ws.display) == -1 && errno != EAGAIN) {
				waypass_fini(&ws);
				eprintf("wl_display_flush: %s\n", strerror(errno));
			}
		} while (errno == EAGAIN);

		if (poll(fds, nfds, -1) < 0) {
			waypass_fini(&ws);
			eprintf("poll: %s\n", strerror(errno));
		}

		if (fds[0].revents & POLLIN) {
			if (wl_display_dispatch(ws.display) < 0) ws.run = false;
		}

		if (fds[1].revents & POLLIN) keyboard_repeat(&ws);
	}

	waypass_fini(&ws);

	return ws.run == RunSuccess ? ExitSuccess : ExitFailure;

print_usage:
	fprintf(stderr, "%s", usage);
	return 1;
}
