static const uint16_t IconHeight = 14;
static const uint16_t IconWidth  = 17;

enum {
	IconPistol,   IconMount,
	IconRhomboid, IconStripes,
	IconSquares,  IconCatSymbol,
	IconAmount,
};

#define O 0xFFFFFFff
#define _ 0x00000000
// array size: 22 x 22 = 484

static const uint32_t PistolBits[] = {
_,O,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
};

static const uint32_t MountBits[] = {
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
};

static const uint32_t RhomboidBits[] = {
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
};

static const uint32_t StripesBits[] = {
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
};

static const uint32_t SquaresBits[] = {
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
};

static const uint32_t CatSymbolBits[] = {
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
};

#undef O
#undef _

static const const uint32_t *Icons[] = {
	PistolBits,   MountBits,
	RhomboidBits, StripesBits,
	SquaresBits,  CatSymbolBits,
};
