static const uint8_t MaxClipboardLen = 160; // Max byte length for clipboard contents
static const char *ClipboardCommand = "waypaste"; // alt: wl-paste

/* Currently does not do anything */
#define UNDERSCORE_CURSOR 1 // 0 for vertical line cursor

#define FONT_NUMBER 4
static const char *FnNames[FONT_NUMBER] = {
	"monospace:pixelsize=13:style=Bold:antialias=true",
	"Koruri:pixelsize=13:Semibold:antialias=true",
	"Blobmoji:pixelsize=13:antialias=true:autohint=true",
	"Symbola:pixelsize=13:antialias=true:autohint=true",
};

static const char Asterisk = '*';

static const uint32_t ColBg     = 0x0C0C0Ced;
static const uint32_t ColPassBg = 0x161616ed;
static const uint32_t ColCursor = 0xEBA28Bff;
static const uint32_t ColBorder = 0x161616ff;
static const uint32_t ColDesc   = 0x777794ff;
static const uint32_t ColNotice = 0xEB8BB2ff;
static const uint32_t ColPrompt = 0xA39CD9ff;
static const uint32_t ColPass   = 0x9292B5ff;
static const uint32_t ColHelp   = 0x5A5977ff;

static const uint32_t ColRune    = 0x777794ff;
static const uint32_t ColRuneAlt = 0xB3B3D5ff;

static const uint32_t ColImageFg      = 0xEBCB8Bed;
static const uint32_t ColImageShading = 0xEBA28Bed;
static const uint32_t ColImageBorder  = 0x404040ed;

static const uint16_t DimLineHeight = 22;
static const uint16_t DimTextLevel  = 16; // text level from top of line
static const uint16_t DimInputWidth = 300;
static const uint8_t  BorderWidth   = 5;

static const char *PromptText = "Passphrase:";
static const char *HelpText = "Ctrl-x: Reveal/Conceal input, Ctrl-c: Cancel";
static const char *HelpTextDisableNaked = "Ctrl-c: Cancel";

static const uint8_t HiddenCharAmount = 18;
static const char *HiddenLenChars = "!@#$%^&*+-=~<>;:.,";

static const double TimeOut = 2.0; // Default timeout before exiting, in seconds
